/*$('.owl-products').owlCarousel({
    loop:true,
    margin:30,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        435:{
            items:2
        },
        767:{
            items:3
        },
        1000:{
            items:5
        }
    }
});
$('.owl-comment').owlCarousel({
    loop:true,
    margin:80,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        767:{
            items:2
        },
        1000:{
            items:2
        }
    }
});
$('.owl-why').owlCarousel({
    loop:true,
    margin:30,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        435:{
            items:2
        },
        767:{
            items:3
        },
        1000:{
            items:4
        }
    }
});

$('.owl-bai-viet').owlCarousel({
    loop:true,
    margin:30,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        360:{
            items:2
        },
        480:{
            items:3
        },
        1000:{
            items:5
        }
    }
});*/



$(document).ready(function() {
     
    $("#owl-banner").owlCarousel({
     
        navigation : false, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        autoPlay: true,
        autoPlay : 3000,
        stopOnHover: true,
        transitionStyle : "fade",
       /* transitionStyle : "fadeUp",
        transitionStyle : "backSlide",*/
       /* transitionStyle : "goDown",*/
        singleItem:true
       
        
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
     
    });
     
});
$(document).ready(function() {
     
    $(".owl-products").owlCarousel({
     
        navigation : true, // Show next and prev buttons
      /*  slideSpeed : 300,
        paginationSpeed : 400,
        autoPlay: true,
        autoPlay : 2500,
        stopOnHover: true,
        singleItem:true,*/
        navigationText: ["",""],
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
        
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
               /* itemsCustom : [
                    [0, 2],
                    [450, 4],
                    [600, 7],
                    [700, 9],
                    [1000, 10],
                    [1200, 12],
                    [1400, 13],
                    [1600, 15]
                  ],*/
     
    });
    $(".owl-doi-tac").owlCarousel({
     
        navigation : true, // Show next and prev buttons
      /*  slideSpeed : 300,
        paginationSpeed : 400,
        autoPlay: true,
        autoPlay : 2500,
        stopOnHover: true,
        singleItem:true,*/
        navigationText: ["",""],
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
        
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
               /* itemsCustom : [
                    [0, 2],
                    [450, 4],
                    [600, 7],
                    [700, 9],
                    [1000, 10],
                    [1200, 12],
                    [1400, 13],
                    [1600, 15]
                  ],*/
     
    });
     
});
$(function () {
    var austDay = new Date();
    /*austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 10);*/
    austDay = new Date(2015, 11-1, 17);
    $('#defaultCountdown').countdown({until: austDay});
    $('#year').text(austDay.getFullYear());
});
