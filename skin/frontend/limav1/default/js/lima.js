jQuery(document).ready(function() {
 jQuery("#block-related").owlCarousel({
      items : 3,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3],
      autoPlay:true,
      stopOnHover : true,
      transitionStyle : false
  });

  jQuery("#home2-owl").owlCarousel({
      items : 1,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      autoPlay:true,
      stopOnHover : true,
      transitionStyle : false
});

  jQuery("#home2-deal-owl").owlCarousel({
      items : 4,
      autoPlay:true,
      stopOnHover : true,
      transitionStyle : false
  });
  jQuery("#home2-new-owl").owlCarousel({
      items : 4,
      autoPlay:true,
      stopOnHover : true,
      transitionStyle : false
  });
  jQuery("#home2-feature-owl").owlCarousel({
      items : 4,
      autoPlay:true,
      stopOnHover : true,
      transitionStyle : false
  });
  jQuery("#home2-trending-owl").owlCarousel({
      items : 4,
      autoPlay:true,
      stopOnHover : true,
      transitionStyle : false
  });


  jQuery("#home2-brand").owlCarousel({
      items : 4,
      stopOnHover : true,
      itemsDesktop : [1199,4],
      itemsDesktopSmall : [980,3],
      itemsTablet: [768,3],
      itemsTabletSmall: false,
      itemsMobile : [479,3],
      autoPlay:false,
      transitionStyle : false
  });

  jQuery("#home2-last-img-owl").owlCarousel({
      items : 4,
      stopOnHover : true,
      itemsDesktop : [1199,4],
      itemsDesktopSmall : [980,3],
      itemsTablet: [768,3],
      itemsTabletSmall: false,
      itemsMobile : [479,3],
      autoPlay:true,
      transitionStyle : false
  });


   jQuery("#m1-review-owl").owlCarousel({
      items : 4,
      stopOnHover : true,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3],
      autoPlay:true,
      transitionStyle : false
  });

});

